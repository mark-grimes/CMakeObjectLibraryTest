#include <stdio.h>
#include "b.h"
#include "obj.h"
#include "a.h"

int main( int argc, char* argv[] )
{
    printf("main started\n");
    function_from_b();
    function_from_obj();
    function_from_a();
    return 0;
}
