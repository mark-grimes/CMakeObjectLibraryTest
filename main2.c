#include <stdio.h>
#include "obj2.h"
#include "obj.h"
#include "a.h"

int main( int argc, char* argv[] )
{
    printf("main2 started\n");
    function_from_obj2();
    function_from_obj();
    function_from_a();
    return 0;
}
