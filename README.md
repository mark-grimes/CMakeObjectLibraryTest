# Test of linking CMake OBJECT libraries

## Update

It turns out this isn't a bug, I just hadn't read the documentation properly. `OBJECT` libraries aren't supposed to link the objects from dependant `OBJECT` libraries.
I find this suprising but it is [in the docs](https://cmake.org/cmake/help/latest/command/target_link_libraries.html#linking-object-libraries), see the [CMake discourse for a post](https://discourse.cmake.org/t/cant-sign-up-to-gitlab-kitware-com-to-report-a-bug/3432) with added emphasis making it clear.

See their [issue #18090](https://gitlab.kitware.com/cmake/cmake/-/issues/18090) explaining the reasoning behind this.

A workaround I've found is to also always add the sources when you link to an `OBJECT` library from another. So in the example in this file the section...
```cmake
add_library(obj2 OBJECT obj2.c)
target_link_libraries(obj2 PUBLIC obj)
```
...would become:
```cmake
add_library(obj2 OBJECT obj2.c)
target_link_libraries(obj2 PUBLIC obj)
target_sources(obj2 PUBLIC $<TARGET_OBJECTS:obj>)
```
This might end in tears though for the reasons it's not done automatically. I've not tried this with diamond dependencies or anything so use at your own risk.

## Previous description

This is a minimal failing example of linking CMake OBJECT libraries in a chain.

The CMake file for this project is taken directly from the CMake documentation for [linking object libraries](https://cmake.org/cmake/help/latest/command/target_link_libraries.html#linking-object-libraries).

The example creates two OBJECT libraries `obj2` and `obj`, where `obj2` links to `obj`. If you link to `obj2` and try to use the symbols from `obj` the symbols are not defined. As I say this is literally the configuration in the documentation, so it should work.

On macOS 11.4, Apple clang 12.0.5 and CMake 3.20.1 I get:
```
Undefined symbols for architecture x86_64:
  "_function_from_obj", referenced from:
      _main in main2.c.o
     (maybe you meant: _function_from_obj2)
ld: symbol(s) not found for architecture x86_64
clang: error: linker command failed with exit code 1 (use -v to see invocation)
make[2]: *** [main2] Error 1
make[1]: *** [CMakeFiles/main2.dir/all] Error 2
```

There is a note in the [documentation for add_library](https://cmake.org/cmake/help/latest/command/add_library.html#object-libraries) that states:

> Some native build systems (such as Xcode) may not like targets that have only object files, so consider adding at least one real source file to any target that references `$<TARGET_OBJECTS:objlib>`.

I don't think this applies in this case because every target has at least one source file.
